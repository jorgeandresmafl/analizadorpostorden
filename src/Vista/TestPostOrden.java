/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Pila;
import java.util.Scanner;

/**
 *
 * @author Jorge Marles
 */
public class TestPostOrden {

    public static void main(String[] args) {
        Pila<Double> posorden = new Pila();
        Scanner sc = new Scanner(System.in);
        //leo el string
        String todo = sc.next();
        //lo convierto a un vector de operadores {"1","3","+"}
        String[] tokens = todo.split(",");
        //este boolean es por un caso loco
        boolean ok = true;
        for (String token : tokens) {
            try {
                //intento pasar el string a un numero
                double number = Double.parseDouble(token);
                //y lo pongo en la pila
                posorden.push(number);
            } catch (NumberFormatException e) {
                //si llega aquí es pq no es un numero (o bien es signo, o  bien es una letra)
                double x = posorden.pop();
                double y = posorden.pop();
                //obtengo los 2 numeros
                try {
                    //aqui caclculo el resultado
                    double res = operar(x, y, token);
                    //pongo la el resultado de la operacion en la pila
                    posorden.push(res);
                } catch (RuntimeException ae) {
                    //si llega aquí es porque o bien hubo divisiones o potencias invalidas
                    //o porque el operador no existía
                    ok = false;
                    System.out.println("Error: " + ae.getMessage());
                }
            }
        }

        try {
            //intento obtener e imprimir el resultado
            double fin = posorden.pop();
            System.out.println("El resultado es " + fin);
        } catch (Exception e) {
            if (ok) {//si llega a esto es por si acaso la pila queda vacía antes de imprimir el resultado
                //dudo mucho que pueda pasar, pero pues xd
                System.out.println("Error: " + e.getMessage() + " antes de terminar el calculo de las operaciones");
            }
        }

        if (!posorden.isVacia()) {
            //algo malió sal (la pila debería estar vacia aquí)
            String prnt = imprimirPila(posorden);
            throw new RuntimeException("La pila no está vacía y las operaciones han terminado: " + prnt);
        }
    }

    private static String imprimirPila(Pila<Double> p) {
        String s = "";
        while (!p.isVacia()) {
            double x = p.pop();
            s += x + " , ";
        }
        return s;
    }

    private static double operar(double y, double x, String op) {
        double res = 0d;
        if (op.equals("+")) {
            res = y + x;
        } else if (op.equals("-")) {
            res = y - x;
        } else if (op.equals("*")) {
            res = y * x;
        } else if (op.equals("/")) {
            if (x == 0) {
                //el denominador es 0
                throw new ArithmeticException("Division entre cero (" + x + "/" + y + ")");
            }
            res = y / x;
        } else if (op.equals("^")) {
            if (y == 0 && x < 0) {
                //esto es por si acaso me mandan, por ejemplo 0 ^ -2, lo que quedaría 1/(0^2)
                
                throw new ArithmeticException("Potencia invalida: La base es 0 y el exponente es negativo: " + y + "^" + x);
            }
            res = Math.pow(y, x);
        } else {
            //si llega aquí es porque la operación no es una operacion matematica, como un signo o letra
            throw new RuntimeException(op + " no es una operacion matematica valida");
        }

        return res;
    }

}
