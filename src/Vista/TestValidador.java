/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;
import Util.seed.Pila;
import java.util.Scanner;
/**
 *
 * @author DOCENTE
 */
public class TestValidador {
    public static void main(String[] args) {
        System.out.println("Validando L={a^n . b^m}^*, donde m=2n");
        /**
         * Ejemplo: L={baabbb} => n=2 , m=4 --> n=2m? --> SI--> V
         */
        Scanner t = new Scanner(System.in);
        System.out.println("Digite la cadena a validar:");
        String s = t.nextLine();
        Pila<Character> pa = new Pila();
        Pila<Character> pb = new Pila();
        for (int i = 0; i < s.length(); i++) {
            switch (s.charAt(i)) {
                case 'a':
                    pa.push('a');
                    break;
                case 'b':
                    pb.push('b');
                    break;
                default:
                    throw new RuntimeException("Caracter invalido");
            }
        }
        boolean sw = true;
        while(!pa.isVacia()&&sw){
            try {
                pa.pop();
                pb.pop();
                pb.pop();
            } catch (Exception e) {
                sw=!sw;
            }
        }
        
        if(pa.isVacia()&&pb.isVacia()&&sw){
            System.out.println("Cumple :)");
        }else{
            System.out.println("No cumple :(");
        }  
    }
}
