/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Conjunto;
import java.awt.BorderLayout;
import java.util.Random;

/**
 *
 * @author Jorge Marles
 */
public class TestConjuntos {

    public static void main(String[] args) {

        Conjunto<Integer> c1 = new Conjunto();//crear(5);
        for(int i = 5; i > 0; i--){
            c1.insertar(i);
        }
        Conjunto<Integer> c2 = crear(5);
        System.out.println("C1: "+c1);
        System.out.println("C2: "+c2);
        Conjunto<Integer> u = c1.getUnion(c2);
        System.out.println("Union: "+u);
        Conjunto<Integer> inter = c1.getInterseccion(c2);
        System.out.println("Interseccion: "+inter);
        Conjunto<Integer> d12 = c1.getDiferencia(c2);
        System.out.println("C1 - C2: "+d12);
        Conjunto<Integer> d21 = c2.getDiferencia(c1);
        System.out.println("C2 - C1: "+d21);
        Conjunto<Integer> dasim = c1.getDiferenciaAsimetrica(c2);
        System.out.println("A difasim B: "+dasim);
        
        
        Conjunto<Conjunto<Integer>> pares = c1.getPotencia();
        System.out.println("Pares:");
        System.out.println(pares);
        
    }
    
    

    private static Conjunto<Integer> crear(int n) {

        if (n <= 0) {
            throw new RuntimeException("No puedo crear lista");
        }
        Conjunto<Integer> l = new Conjunto();
        while (n > 0) {
            l.insertar(new Random().nextInt(n*10));
            n--;
        }
        return l;
    }
}
