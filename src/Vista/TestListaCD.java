/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaCD;
import java.util.Iterator;
import java.util.Random;

/**
 *
 * @author DOCENTE
 */
public class TestListaCD {

    public static void main(String[] args) {
        ListaCD<Integer> l = arrayToListaCD(new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15});
        System.out.println(l);
        
        
    }
    
    private static ListaCD<Integer> arrayToListaCD(int[] elementos){
        ListaCD<Integer> lcd = new ListaCD<>();
        for(int elemento : elementos)lcd.insertarFin(elemento);
        return lcd;
    }

    private static ListaCD<Integer> crearInicio(int n) {

        if (n <= 0) {
            throw new RuntimeException("No puedo crear lista");
        }
        ListaCD<Integer> l = new ListaCD();
        while (n > 0) {
            l.insertarInicio(new Random().nextInt(n));
            n--;
        }
        return l;
    }

    private static ListaCD<Integer> crearFin(int n) {

        if (n <= 0) {
            throw new RuntimeException("No puedo crear lista");
        }
        ListaCD<Integer> l = new ListaCD();
        int i = 1;
        while (i <= n) {
            l.insertarFin(i);
            i++;
        }
        return l;
    }
}
