/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.Pila;
import java.util.Scanner;

/**
 *
 * @author DOCENTE
 */
public class TestValidador_Agrupamiento {

    public static void main(String[] args) {
        System.out.println("Validando [{()}] ");
        /**
         * Ejemplo: L={aa} => --> V L={[aa]} => --> F L=(a)[a]{b}--> V
         */
        Scanner t = new Scanner(System.in);
        String s = t.nextLine();
        char[] openers = {'[', '{', '('};
        char[] closers = {']', '}', ')'};
        int it = 0;
        Pila<Character> pc = new Pila();
        boolean ok = true;

        for (int i = 0; ok && i < s.length(); i++) {
            char c = s.charAt(i);
            int posCloser = contains(c, closers);
            int posOpener = contains(c, openers);
            if (posCloser == -1 && posOpener == -1) {
                continue;
            }
            if (posCloser != -1) {
                try {
                    if (posCloser == it) {
                        pc.pop();
                        it--;
                    } else {
                        ok = false;
                    }
                } catch (Exception e) {
                    ok = false;
                }
            } else {
                if (pc.isVacia() || posOpener == it + 1) {
                    pc.push(c);
                    it = posOpener;
                } else {
                    ok = false;
                }

            }
        }
        if (ok && pc.isVacia()) {
            System.out.println("Valido");
        } else {
            System.out.println("No valido");
        }
    }

    private static int contains(char c, char[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == c) {
                return i;
            }
        }
        return -1;
    }
}
