/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

import java.util.Iterator;

/**
 *
 * @author DOCENTE
 */
public class ListaCD<T> implements Iterable<T> {

    private NodoD<T> cabeza;
    private int size = 0;

    public int getSize() {
        return size;
    }

    public ListaCD() {

        this.cabeza = new NodoD(null, null, null);
        this.cabeza.setAnt(cabeza);
        this.cabeza.setSig(cabeza);
    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza.getSig(), this.cabeza);
        this.cabeza.setSig(nuevo);
        nuevo.getSig().setAnt(nuevo);
        this.size++;
    }

    public void insertarFin(T info) {
        NodoD<T> nuevo = new NodoD(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.size++;
    }

    @Override
    public String toString() {
        String msg = "";
        for (NodoD<T> x = this.cabeza.getSig(); x != this.cabeza; x = x.getSig()) {
            msg += x.getInfo() + "<->";
        }
        return "cab<->" + msg + "cab";
    }

    public boolean isVacia() {
        //this.size==0
        //this.cabeza==this.cabeza.getAnt()
        return this.cabeza == this.cabeza.getSig();
    }

    public T remove(int i) {
        validar(i);
        NodoD<T> borrar = this.getPos(i);
        borrar.getAnt().setSig(borrar.getSig());
        borrar.getSig().setAnt(borrar.getAnt());
        this.size--;
        borrar.setAnt(null);
        borrar.setSig(null);
        return borrar.getInfo();
    }

    private void validar(int i) {
        if (this.isVacia() || i < 0 || i >= this.size) {
            throw new RuntimeException("No es válida la posición");
        }
    }

    private NodoD<T> getPos(int posicion) {
        this.validar(posicion);
        NodoD<T> x = this.cabeza.getSig();
        // a=3, b=a; b=3
        while (posicion-- > 0) {

            x = x.getSig();
            //pos=pos-1 : --> first class OP :(
        }
        return x;
    }

    public T get(int i) {
        try {
            this.validar(i);
            NodoD<T> pos = this.getPos(i);
            return pos.getInfo();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public void set(int i, T info) {
        try {
            this.validar(i);
            NodoD<T> pos = this.getPos(i);
            pos.setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorLCD(this.cabeza);
    }

    //interseccion de conjuntos
    public ListaCD<T> getInterseccion(ListaCD<T> l1) {
        if (this.isVacia() || l1.isVacia()) {
            throw new RuntimeException("Al menos una de las listas esta vacia");
        }
        ListaCD<T> result = new ListaCD();
        ListaCD<T> mayor = this;
        ListaCD<T> menor = l1;
        if (this.size < l1.size) {
            mayor = l1;
            menor = this;
        }
        Iterator<T> it = menor.iterator();
        while (it.hasNext()) {
            T info = it.next();
            if (mayor.contains(info)) {
                result.insertarFin(info);
            }
        }
        return result;
    }

    public boolean contains(T info) {
        Iterator<T> it = this.iterator();
        while (it.hasNext()) {
            if (it.next().equals(info)) {
                return true;
            }
        }
        return false;
    }

    public void concat(ListaCD<T> l1, int i) {
        //l1 queda vacío
        //no crear nodos
        //inserta despues de i
        validar(i);
        if (l1.isVacia()) {
            throw new RuntimeException("La lista l1 no puede estar vacía ");
        }
        NodoD<T> insertar = this.getPos(i);
        NodoD<T> colaL1 = l1.cabeza.getAnt();

        //conectamos el final de la segunda lista
        colaL1.setSig(insertar.getSig());
        insertar.getSig().setAnt(colaL1);

        //conectamos el inicio de la segunda lista
        insertar.setSig(l1.cabeza.getSig());
        l1.cabeza.getSig().setAnt(insertar);

        l1.clear();
    }

    public void clear() {
        this.cabeza.setAnt(cabeza);
        this.cabeza.setSig(cabeza);
    }

    /**
     * Método que permite operar con 2 listas ordenadas ascendentemente y
     * retorna la intersección en otra lista ordenada
     *
     * @param l1 Una lista ordenada
     * @return Una lista tipo T
     */
    /*
    public ListaCD<T> getInterseccionOrdenado(ListaCD<T> l1) {
        if (this.isVacia() || l1.isVacia()) {
            throw new RuntimeException("Las listas no pueden estar vacias");
        }
        if (!(this.isSorted(true) && l1.isSorted(true))) {
            throw new RuntimeException("Las listas deben estar ordenadas ascendentemente");
        }

        return this.getInterseccion(l1);
    }
*/
    /**
     * *
     * Verifica si una lista está ordenada
     *
     * @param orden determina si se debe verificar que la lista está ordenada
     * ascendente o descendentemente:
     * <ul>
     * <li> <code>true</code> para verificar si la lista está ordenada
     * ascendentemente </li>
     * <li> <code>false</code> para verificar si la lista está ordenada
     * descendentemente </li>
     * </ul>
     * @return
     */
    /*
    public boolean isSorted(boolean orden) {
        //si this es menor que sig (ascendentemente) compareTo < 0
        //si this es mayor que sig (descendentemente) compareTo > 0
        if (this.isVacia()) {
            return false;
        }
        int verificador = (orden) ? -1 : 1;
        boolean ok = true;
        Iterator<T> it = this.iterator();
        Iterator<T> it2 = this.iterator();
        it2.next();
        while (ok && it2.hasNext()) {
            T info1 = it.next();
            T info2 = it2.next();
            int resultado = info1.compareTo(info2);
            ok = (resultado == 0) || ((resultado < 0) == (verificador < 0));

            //System.out.println("Comparando " + info1 + " con " + info2);
        }
        return ok;
    }
     */
    /**
     * Obtiene verdadero o falso si un elemento existe en una lista ordenada
     * ascendentemente No usar iterador
     *
     * @param elemento
     * @return
     */
    /*
    public boolean contains_sort(T elemento) {
        if (!this.isSorted(true)) {
            throw new RuntimeException("La lista no está ordenada ascendentemente");
        }
        T min = this.cabeza.getSig().getInfo();
        T max = this.cabeza.getAnt().getInfo();
        if (elemento.compareTo(min) < 0 || elemento.compareTo(max) > 0) {
            return false;
        }

        int mid = this.size / 2;
        NodoD<T> inicio = this.cabeza.getSig();
        NodoD<T> fin = getPos(mid);
        if (elemento.compareTo(fin.getInfo()) > 0) {
            inicio = fin;
            fin = this.cabeza;
        }

        for (NodoD<T> it = inicio; it != fin; it = it.getSig()) {
            if (it.getInfo().equals(elemento)) {
                return true;
            }
        }

        return false;
    }
*/
}
