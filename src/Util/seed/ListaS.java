/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util.seed;

import java.util.Iterator;
import java.util.Objects;

/**
 *
 *
 * @author Estudiantes
 */
public class ListaS<T> implements Iterable<T> {

    private Nodo<T> cabeza;
    private int size;

    public ListaS() {
        this.cabeza = null;
        this.size = 0;
    }

    public int getSize() {
        return size;
    }

    public void insertarInicio(T info) {

        this.cabeza = new Nodo(info, this.cabeza);
        this.size++;
    }

    public void insertarFin(T info) {
        if (this.isVacia()) {
            this.insertarInicio(info);
        } else {
            getPos(this.size - 1).setSig(new Nodo(info, null));
            this.size++;
        }
    }

    private void insertarTras(Nodo<T> anterior, T info) {
        Nodo<T> insert = new Nodo(info, anterior.getSig());
        anterior.setSig(insert);
        this.size++;
    }

    private Nodo<T> getPos(int posicion) {
        this.validar(posicion);
        Nodo<T> x = this.cabeza;
        // a=3, b=a; b=3
        while (posicion-- > 0) {

            x = x.getSig();
            //pos=pos-1 : --> first class OP :(
        }
        return x;
    }

    private void validar(int i) {
        if (this.isVacia() || i < 0 || i >= this.size) {
            throw new RuntimeException("No es válida la posición");
        }
    }

    @Override
    public String toString() {
        if (this.isVacia()) {
            return "Lista Vacía";
        }
        String msg = "";
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            msg += i.toString() + " -> ";
        }
        return msg + "null";

    }

    public String toRawString() {
        if (this.isVacia()) {
            return "";
        }
        String msg = "";
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            msg += i.getInfo().toString() + "";
        }
        return msg;

    }

    public boolean isVacia() {
        return this.cabeza == null;
    }

    public T get(int i) {
        try {
            this.validar(i);
            Nodo<T> pos = this.getPos(i);
            return pos.getInfo();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    public void set(int i, T info) {
        try {
            this.validar(i);
            Nodo<T> pos = this.getPos(i);
            pos.setInfo(info);
        } catch (Exception e) {
            System.err.println(e.getMessage());

        }
    }

    public T remove(int i) {
        this.validar(i);
        Nodo<T> borrar = this.cabeza;
        if (i == 0) {
            this.cabeza = this.cabeza.getSig();

        } else {
            Nodo<T> anterior = this.getPos(i - 1);
            borrar = anterior.getSig();
            anterior.setSig(borrar.getSig());
        }

        this.size--;
        borrar.setSig(null);
        return borrar.getInfo();
    }

    public boolean addAll(ListaS<T> l2) {
        if (this.isVacia() && l2.isVacia()) {
            return false;
        }

        if (l2.isVacia()) {
            return false;
        }

        if (this.isVacia()) {
            this.cabeza = l2.cabeza;

        } else {
            Nodo<T> ultimo = this.getPos(size - 1);
            ultimo.setSig(l2.cabeza);

        }

        this.size += l2.size;
        l2.clear();
        return true;
    }
    
    public boolean addAllSinEliminar(ListaS<T> l2) {
        if (this.isVacia() && l2.isVacia()) {
            return false;
        }

        if (l2.isVacia()) {
            return false;
        }

        if (this.isVacia()) {
            this.cabeza = l2.cabeza;

        } else {
            Nodo<T> ultimo = this.getPos(size - 1);
            ultimo.setSig(l2.cabeza);

        }

        this.size += l2.size;
        return true;
    }

    public void clear() {
        this.cabeza = null;
        this.size = 0;
    }

    public boolean isPalin() {
        if (this.isVacia()) {
            throw new RuntimeException("No se puede realizar el proceso");
        }

        ListaS<T> l2 = crearInvertida();
        return (this.equals(l2));

    }

    public ListaS<T> crearInvertida() {
        if (this.isVacia()) {
            throw new RuntimeException("No es válido operación");
        }
        ListaS<T> l2 = new ListaS();
        Nodo<T> ultimo_l2 = null;
        for (Nodo<T> actual = this.cabeza; actual != null; actual = actual.getSig()) {
            T info = actual.getInfo();
            Nodo<T> nuevo_l2 = new Nodo(info, ultimo_l2);
            ultimo_l2 = nuevo_l2;
        }
        l2.cabeza = ultimo_l2;
        l2.size = this.size;
        return l2;

    }

    public void invertir() {
        if (this.isVacia()) {
            throw new RuntimeException("No es válido operación");
        }
        if (this.size == 1) {
            return;
        }

        Nodo<T> ult = null;
        Nodo<T> actual = this.cabeza;
        Nodo<T> sig;
        while (actual != null) {
            sig = actual.getSig();
            actual.setSig(ult);
            ult = actual;
            actual = sig;

        }

        this.cabeza = ult;

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.cabeza);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ListaS<T> other = (ListaS<T>) obj;
        
        if (this.isVacia() || other.isVacia()) {
            return false;
        }
        
        if (this.size != other.size) {
            return false;
        }
        // T debe tener implementado equals
        for (Nodo<T> c = this.cabeza, c2 = other.cabeza; c != null; c = c.getSig(), c2 = c2.getSig()) {
            if (!c.getInfo().equals(c2.getInfo())) {
                return false;
            }
        }
        return true;
    }

    public boolean contieneRepetidos() {
        Nodo<T> x = this.cabeza;
        while (x.getSig() != null) {
            Nodo<T> y = x.getSig();
            while (y.getSig() != null) {
                if (y.equals(x)) {
                    return true;
                }
                y = y.getSig();
            }
            x = x.getSig();
        }
        return false;
    }

    public void eliminarTodosRepetidos() {
        Nodo<T> x = this.cabeza;
        Nodo<T> anterior = this.cabeza;
        while (x != null && x.getSig() != null) {
            Nodo<T> aux = x;
            boolean foundRepeated = false;
            Nodo<T> y = x;
            while (y != null && y.getSig() != null) {
                //System.out.println("Comparando x:"+x.getInfo()+" y y:"+y.getSig().getInfo()+" da "+foundRepeated);
                if (y.getSig().equals(x)) {
                    foundRepeated = true;
                    y.setSig(y.getSig().getSig());
                    this.size--;
                } else {
                    y = y.getSig();
                }
                //System.out.println("Tras iteracion, queda ");
                //System.out.println(this);
            }
            if (foundRepeated) {
                if (x == this.cabeza) {
                    this.cabeza = x.getSig();
                } else {
                    anterior.setSig(x.getSig());
                }
                this.size--;
            } else {
                anterior = x;

            }
            x = x.getSig();

        }
    }

    public void eliminarRepetidos() {
        Nodo<T> x = this.cabeza;
        while (x != null && x.getSig() != null) {
            Nodo<T> y = x;
            while (y != null && y.getSig() != null) {
                //System.out.println("Comparando x:"+x.getInfo()+" y y:"+y.getSig().getInfo()+" da "+foundRepeated);
                if (y.getSig().equals(x)) {
                    y.setSig(y.getSig().getSig());
                    this.size--;
                } else {
                    y = y.getSig();
                }
                //System.out.println("Tras iteracion, queda ");
                //System.out.println(this);
            }
            x = x.getSig();
        }
    }

    public void replace(ListaS<T> buscar, ListaS<T> reemplazar) {
        if (this.isVacia() || buscar.isVacia()) {
            throw new RuntimeException("Ni la lista original ni la lista a buscar pueden estar vacias");
        }
        for (Nodo<T> actual = this.cabeza, anterior = actual; actual != null; actual = actual.getSig()) {
            //System.out.println("Antes\n"+this);
            //System.out.println("Actual(antes de buscar): "+actual);
            Nodo<T> fin = buscar(actual, buscar);

            if (fin != null) {
                //System.out.println("Ocurrencia con actual: " + actual + ", fin: " + fin);
                //copiaActual tiene el fin de la ocurrencia
                //actual tiene el inicio
                //anterior tiene el anterior al inicio
                anterior = addInNode(actual, fin, anterior, reemplazar);
                this.size -= buscar.size;
                this.size += reemplazar.size;
                actual.setSig(fin.getSig());
            } else {
                anterior = actual;
            }
            //System.out.println("Despues\n"+this+"\n\n");
        }
    }

    private Nodo<T> addInNode(Nodo<T> actual, Nodo<T> fin, Nodo<T> anterior, ListaS reemplazar) {
        //System.out.println("Cabeza: "+this.cabeza+" Actual: "+actual+", fin: "+fin+", anterior: "+anterior);
        ListaS<T> clon = reemplazar.clone();
        Nodo<T> cola = clon.tail();
        if (actual == this.cabeza) {
            this.cabeza = clon.cabeza;
            cola.setSig(fin.getSig());
        } else {
            anterior.setSig(clon.cabeza);
            cola.setSig(fin.getSig());
        }

        return cola;
    }

    private Nodo<T> buscar(Nodo<T> actual, ListaS buscar) {
        boolean igual = true;
        Nodo<T> fin = actual;
        for (Nodo<T> buscarNodo = buscar.cabeza;
                igual && buscarNodo != null;
                buscarNodo = buscarNodo.getSig()) {
            if (!buscarNodo.equals(fin)) {
                igual = false;
            } else if (buscarNodo.getSig() != null) {
                fin = fin.getSig();
            }

        }
        if (igual) {
            return fin;
        } else {
            return null;
        }
    }

    private Nodo<T> tail() {
        return this.getPos(size - 1);
    }

    public ListaS<T> clone() {
        if (isVacia()) {
            throw new RuntimeException("Error de Clonacion: Lista Vacia");
        }
        ListaS<T> other = new ListaS();
        Nodo<T> orig = this.cabeza;
        while (orig != null) {
            other.insertarFin(orig.getInfo());
            orig = orig.getSig();
        }
        return other;
    }

    public ListaS<T> unionConjuntos(ListaS<T> l2) {
        ListaS<T> union = new ListaS();
        ListaS<T> clonThis = this.clone();
        ListaS<T> clonL2 = l2.clone();
        union.addAll(clonThis);
        union.addAll(clonL2);
        union.eliminarRepetidos();
        return union;
    }

    public boolean contains(T info) {
        for (Nodo<T> it = this.cabeza; it != null; it = it.getSig()) {
            if (it.getInfo().equals(info)) {
                return true;
            }
        }
        return false;
    }

    public ListaS<T> interseccionConjuntos(ListaS<T> l2) {
        Nodo<T> head = null;
        Nodo<T> fin = head;

        for (Nodo<T> este = this.cabeza; este != null; este = este.getSig()) {
            if (l2.contains(este.getInfo())) {
                if (head == null) {
                    head = new Nodo(este.getInfo(), null);
                    fin = head;
                } else {
                    Nodo<T> last = new Nodo(este.getInfo(), null);
                    fin.setSig(last);
                    fin = last;
                }
            }
            //System.out.println(este);
        }
        ListaS<T> interseccion = new ListaS<T>();
        interseccion.cabeza = head;

        return interseccion;
    }

    public ListaS<T> diferenciaConjuntos(ListaS<T> l2) {
        Nodo<T> head = null;
        Nodo<T> fin = head;

        for (Nodo<T> este = this.cabeza; este != null; este = este.getSig()) {
            if (!l2.contains(este.getInfo())) {
                if (head == null) {
                    head = new Nodo(este.getInfo(), null);
                    fin = head;
                } else {
                    Nodo<T> last = new Nodo(este.getInfo(), null);
                    fin.setSig(last);
                    fin = last;
                }
            }
        }
        ListaS<T> interseccion = new ListaS<T>();
        interseccion.cabeza = head;
        return interseccion;
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorLS<>(this.cabeza);
    }

}
