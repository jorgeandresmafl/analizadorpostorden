/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

import java.util.Objects;

/**
 * Wrapper (Envoltorio)
 *
 * @author docente
 */
public class Conjunto<T> implements IConjunto<T> {

    private ListaS<T> elementos;

    public Conjunto() {
        this.elementos = new ListaS();
    }

    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        Conjunto<T> union = new Conjunto();
        Conjunto<T> menor = null;
        Conjunto<T> mayor = null;
        if (c1.size() < this.size()) {
            menor = c1;
            mayor = this;
        } else {
            menor = this;
            mayor = c1;
        }
        union.addAll(mayor);
        for (T i : menor.elementos) {
            if (!mayor.contains(i)) {
                union.insertar(i);
            }
        }
        return union;
    }

    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        Conjunto<T> interseccion = new Conjunto<>();
        for (T i : this.elementos) {
            if (c1.contains(i)) {
                interseccion.insertar(i);
            }

        }
        return interseccion;
    }

    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        Conjunto<T> interseccion = new Conjunto<>();
        for (T i : this.elementos) {
            if (!c1.contains(i)) {
                interseccion.insertar(i);
            }
        }
        return interseccion;
    }

    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        Conjunto<T> dasim = new Conjunto();
        for (T i : this.elementos) {
            if (!c1.contains(i)) {
                dasim.insertar(i);
            }
        }
        for (T i : c1.elementos) {
            if (!this.contains(i)) {
                dasim.insertar(i);
            }
        }
        return dasim;
        //return getDiferencia(c1).getUnion(c1.getDiferencia(this));
    }

    @Override
    // {2,3,4} --> conjunto par: {2},{3},{4},{2,3},{3,4},{2,4},{2,3,4},{*}
    public Conjunto<Conjunto<T>> getPotencia() {
        Conjunto<Conjunto<T>> pares = new Conjunto();

        //La cantidad de combinaciones de n elementos de un conjunto de m elementos es 
        //el enésimo elemento de la emésima fila del triangulo de Pascal
        //El dato anterior resultó ser totalmente innecesario, pero lo dejo porque me pareció interesante
        
        for (int sz = 0; sz <= this.size(); sz++) {
            for(int i = 0; i < this.size(); i++){
                buscar(i, sz, this.size(), new Conjunto(), pares); 
            }
        }
        for(Conjunto<T> conjunto : pares.elementos){
            conjunto.elementos.invertir();
        }
        pares.elementos.invertir();
        pares.insertar(new Conjunto());//conjunto vacio
        
        //Este proceso es meramente para imprimir, ya que si solo imprimo el toString de conjunto, queda todo en una sola linea
        for(Conjunto<T> conjunto : pares.elementos){
            System.out.println("Conjunto: "+conjunto);
        }
        
        
        return pares;
    }
    
    private void buscar(int index, int s, int n, Conjunto<T> candidato, Conjunto<Conjunto<T>> ans) {
        candidato.insertar(this.get(index));

        if (candidato.size() == s) {
            //System.out.println("Candidato lo logró: "+candidato);
            ans.insertar(candidato);
            return;
        }
        if (index == n) {
            //System.out.println("Index llegó a n, con candidato siendo "+candidato);
            return;
        }
        //System.out.println("Se inicia bucle con i = "+(index+1));
        for (int i = index + 1; i < n; i++) {
            //System.out.println("Candidato es " + candidato+ ", con index = "+index+" y con i = "+i);
            buscar(i, s, n, candidato.clonar(), ans);
        }
        //System.out.println("Acaba el ciclo cuando index era "+index);
    }
    
    private Conjunto<T> clonar(){
        if(this.isEmpty())return new Conjunto<T>();
        Conjunto<T> clon = new Conjunto();
        
        for(T i : this.elementos){
            clon.insertar(i);
        }
        clon.elementos.invertir();
        return clon;
    }
    
    @Override
    public T get(int i) {
        return this.elementos.get(i);
    }

    @Override
    public void set(int i, T info) {

        this.elementos.set(i, info);
        this.eliminarRepetidos();
    }

    public int size() {
        return this.elementos.getSize();
    }
    
    public boolean isEmpty(){
        return this.elementos.isVacia();
    }

    @Override
    public void insertar(T info) {
        if (!this.contains(info)) {
            this.elementos.insertarInicio(info);
        }
    }
    

    public void addAll(Conjunto<T> c1) {
        this.elementos.addAllSinEliminar(c1.elementos);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conjunto<T> other = (Conjunto<T>) obj;
        return this.elementos.equals(other.elementos);
    }

    @Override
    public String toString() {
        String s = "{";
        // 4 2 56 3 2
        for(T elemento : this.elementos){
            s += elemento+" ";
        }
        s += "}";
        return s;
    }

    public boolean contains(T info) {
        return this.elementos.contains(info);
    }

    public void eliminarRepetidos() {
        this.elementos.eliminarRepetidos();
    }
}
