/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 *
 * @author madarme
 */

public class Pila<T> {

    private ListaCD<T> lista = new ListaCD();

    public Pila() {

    }

    public void push(T info) {
        this.lista.insertarInicio(info);
    }

    public T pop() {
        if (this.lista.isVacia()) {
            throw new RuntimeException("Pila Vacía");
        }
        return this.lista.remove(0);
    }

    public boolean isVacia() {
        return this.lista.isVacia();
    }

    public int size() {
        return this.lista.getSize();
    }

}
